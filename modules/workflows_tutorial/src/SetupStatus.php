<?php

namespace Drupal\workflows_tutorial;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Symfony\Component\Yaml\Yaml;

class SetupStatus {

  /**
   * The module extension list.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The module extension list.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  public function __construct(ModuleExtensionList $moduleExtensionList, ConfigFactoryInterface $configFactory) {
    $this->moduleExtensionList = $moduleExtensionList;
    $this->configFactory = $configFactory;
  }

  /**
   * Get config.
   *
   * @return array
   *   The config.
   */
  public function getConfig() {
    $yaml = file_get_contents($this->moduleExtensionList->getPath('workflows_tutorial') . '/internal/forced_settings.yml');
    return Yaml::parse($yaml);
  }

  /**
   * Check if the setup is complete.
   *
   * @return bool
   *   TRUE if the setup is complete.
   */
  public function isComplete() {
    $config = $this->getConfig();
    if (isset($config['forced_settings'])) {
      foreach ($config['forced_settings'] as $parts) {
        $value = $this->configFactory->getEditable($parts['config_space'])->get($parts['key']);
        if (empty($value)) {
          return FALSE;
        }
      }
    }
    return TRUE;
  }

}
